package com.topic.subscription.model;

import javax.persistence.*;

@Entity
@Table(name = "subsription")
public class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "topic")
    private String topic;
    @Column(name = "url")
    private String url;


    public Subscription() {
    }

    public Subscription(String topic, String url) {
        this.topic = topic;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Subscription [id=" + id + ", topic=" + topic + ", url=" + url  + "]";
    }
}