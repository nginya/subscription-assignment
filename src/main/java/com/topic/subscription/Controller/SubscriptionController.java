package com.topic.subscription.Controller;

import com.topic.subscription.model.Subscription;
import com.topic.subscription.repository.SubscriptionRepository;
import org.json.simple.parser.JSONParser;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.springframework.web.client.RestTemplate;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

public class SubscriptionController {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionController.class);
    @Autowired
    SubscriptionRepository subRepository;
    //Get created subscriptions from postgres database
    @GetMapping("/subscriptions")
    public ResponseEntity<List<Subscription>>getAllSubscribtions(){
        try{
            List<Subscription>  subscriptions = new ArrayList<Subscription>();
                subRepository.findAll().forEach(subscriptions::add);

            if(subscriptions.isEmpty()){
                return new ResponseEntity<>(HttpStatus.OK);
            }
            return new ResponseEntity<>(subscriptions,HttpStatus.OK);
        }
        catch (Exception e){
            logger.error("Error:{}", e);
            return  new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    // Create a subscription for topic {topic}
    @PostMapping(path = "subscribe/{topic}")
    public ResponseEntity<Subscription> createSubscription(@RequestBody Subscription subscription, @PathVariable("topic") String topic){
        try{
            Subscription _sub = subRepository.save(new Subscription(topic, subscription.getUrl()));
            return new ResponseEntity<>(_sub,HttpStatus.OK);
        }catch (Exception e){
            logger.error("Error:{}", e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Publish a topic( Get message from javascript server and publish it to subscribers)
    @PostMapping(path = "publish/{topic}")
    public ResponseEntity<?> publishMsg(@RequestBody String req, @PathVariable("topic") String topic){
        try{
            //Get all subscribers for {topic} from postgres database
            List<Subscription>  subscriptions = new ArrayList<>();
            subRepository.findSubscriptionByTopic(topic).forEach(subscriptions::add);
            //For each of the subscriber, publish message from javascript
            SubscriptionResponse response = new SubscriptionResponse(
                    0,
                    "",
                    ""
            );
            for(Subscription sub: subscriptions){
                RestTemplate restTemplate = new RestTemplate();
                //Get subscriber url
                final String baseUrl = sub.getUrl();
                URI uri = new URI(baseUrl);
                SubscribersPayload payload= new SubscribersPayload(topic,req);
                ResponseEntity<String> result = restTemplate.postForEntity(uri, payload, String.class);
                logger.info("Response status code:{}",result.getStatusCodeValue());

                if(result.getStatusCodeValue()==200){
                    //if success return http response
                     response = new SubscriptionResponse(
                            result.getStatusCodeValue(),
                            "Success",
                            "topic published successfully"
                            );
                   // logger.info("Success:{}",response);
                }
                else {
                    //Else  return error
                    response = new SubscriptionResponse(
                            result.getStatusCodeValue(),
                            "Fail",
                            "topic publish failed"
                    );
                 //   logger.error("Error:{}",response);
                }
            }
            return new ResponseEntity<>(response,HttpStatus.OK);

        }catch (Exception e){
            logger.error("Error:{}", e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Sample testing Url for subscriber servers
    //When  creating subscription, use url  http://localhost:8080/test/{subscriberName} where {subscriberName} is the name of subscribing server
    //example http://localhost:8080/subscribe/topic0
    @PostMapping(path = "test/{subscriberName}")
    public ResponseEntity<?> testSubscription(@RequestBody String request,
                                              @PathVariable("subscriberName") String name){
        try{
            logger.info("Subscriber server name: {}", name);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(request);
            logger.info("Received message::{}", json);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            logger.error("Error::{}", e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
class SubscribersPayload{
    String topic;
    String data;

    public SubscribersPayload(String topic, String data) {
        this.topic = topic;
        this.data = data;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
class SubscriptionResponse{
    int statusCode;
    String message;
    String customerMessage;

    public SubscriptionResponse(int statusCode, String message, String customerMessage) {
        this.statusCode = statusCode;
        this.message = message;
        this.customerMessage = customerMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCustomerMessage() {
        return customerMessage;
    }

    public void setCustomerMessage(String customerMessage) {
        this.customerMessage = customerMessage;
    }
}