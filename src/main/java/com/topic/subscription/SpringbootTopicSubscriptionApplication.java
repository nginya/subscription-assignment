package com.topic.subscription;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootTopicSubscriptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTopicSubscriptionApplication.class, args);
    }

}
